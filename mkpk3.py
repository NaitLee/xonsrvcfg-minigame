#!/usr/bin/env python3

import os, sys, zipfile, uuid

pk3dirs = sys.argv[1:]

if len(pk3dirs) == 0:
    pk3dirs = filter(lambda x: x.endswith('.pk3dir'), os.listdir())

for pk3dir in pk3dirs:
    pk3name = os.path.basename(pk3dir[:-6]) + str(uuid.uuid4())[0:8] + '.pk3'
    with zipfile.ZipFile(pk3name, 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as z:
        os.chdir(pk3dir)
        for path, dirs, files in os.walk('.'):
            for i in dirs + files:
                z.write(os.path.join(path, i), os.path.join(path, i))
        if '-serveronly' not in pk3name:
            z.writestr(pk3name + '.serverpackage', pk3name)
        os.chdir('..')
