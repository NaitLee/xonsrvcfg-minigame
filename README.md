# xonsrvcfg-minigame

Xonotic server configurations dedicated to minigame maps for FSFans.

Also see [main server configurations](https://codeberg.org/fsfans-cn/xonsrvcfg) to mix something in.

Unless otherwise noticed, no rights reserved to these files.
